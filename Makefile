
# For tests
TEZOS_HOME?=~/dev/nomadic-labs/tezos
CONTRACTS_DIR=${TEZOS_HOME}/tests_python/contracts


package:
	python3 setup.py bdist_wheel

upload:
	python3 -m twine upload dist/pygments_michelson-1.0.1-py3-none-any.whl

.PHONY: test
test:
	./test/test-lexer.sh ${CONTRACTS_DIR}
