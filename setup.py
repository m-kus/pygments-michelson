import setuptools, os

basepath = os.path.dirname(__file__)
basepath += '/' if basepath != '' else ''

with open(basepath + "README.md", "r") as fh:
    long_description = fh.read()

requirements = []

setuptools.setup(
    name='pygments-michelson',
    version='1.0.1',
    packages=setuptools.find_packages(basepath + 'src'),
    package_dir={'': basepath + 'src'},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License"
    ],
    url='https://gitlab.com/arvidnl/pygments-michelson',
    project_urls={

    },
    keywords='pygments michelson tezos lexer',
    license='MIT',
    author='Arvid Jakobsson',
    author_email='arvid.jakobsson@nomadic-labs.com',
    description='Pygments lexer for Michelson contracts',
    long_description=long_description,
    long_description_content_type="text/markdown",

    install_requires=['Pygments >= 2'],
    python_requires='>=3.4',

    package_data={
    },
    entry_points={
        'pygments.lexers': [
            'MichelsonLexer = pygments_michelson:MichelsonLexer'
        ]
    },

)
